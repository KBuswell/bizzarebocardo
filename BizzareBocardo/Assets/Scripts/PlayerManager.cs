﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public Character characterPrefab;
    public Enemy currentEnemy;
    public List<Character> party;
    public List<Card> deck;
    public List<Card> hand;
    public List<Card> discard;
    private int maxPartyHealth;
    public int currentPartyHealth;
    public int shield;
    public Text playerHealth;
    public Text enemyHealth;
    public Text enemyMessage;
    public Text loseMessage;
    public Text playerSheild;
    public Text enemySheild;
    public float HBN;
    public CharacterGenerator gen;



    // Start is called before the first frame update
    void Start()
    {
        maxPartyHealth = 0;
        party.Add(gen.generateCharacter());
        foreach (Character c in party){
            maxPartyHealth += c.maxHealth;
            foreach(Card d in c.cardSet)
            {
                deck.Add(d);
            }
        }
        currentPartyHealth = maxPartyHealth;
        shuffle();
        newHand();
        playerHealth.text = "Player Health: " + currentPartyHealth + "/" + maxPartyHealth;
        playerSheild.text = "Player Sheild: " + shield;
        enemyHealth.text = "Enemy Health: " + currentEnemy.currentHealth + "/" + currentEnemy.maxHealth;
        enemySheild.text = "Enemy Sheild: " + currentEnemy.shield;
        HBN = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)&&currentPartyHealth>0)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Card")
                {
                    if (hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite != null)
                    {
                        if(hit.collider.gameObject.name == hand[0].gameObject.name)
                        {
                            playCard(hand[0]);
                            hand.Remove(hand[0]);
                            drawCard();
                            enemyMessage.text = generateEnemyMessage(currentEnemy, currentEnemy.enemyCard(this));
                            
                        }
                        else if (hit.collider.gameObject.name == hand[1].gameObject.name)
                        {
                            playCard(hand[1]);
                            hand.Remove(hand[1]);
                            drawCard();
                            enemyMessage.text = generateEnemyMessage(currentEnemy, currentEnemy.enemyCard(this));
                            currentEnemy.enemyCard(this);
                        }
                        else if (hit.collider.gameObject.name == hand[2].gameObject.name)
                        {
                            playCard(hand[2]);
                            hand.Remove(hand[2]);
                            drawCard();
                            enemyMessage.text = generateEnemyMessage(currentEnemy, currentEnemy.enemyCard(this));
                            currentEnemy.enemyCard(this);
                        }
                    }
                    
                }
            }
            playerHealth.text = "Player Health: " + currentPartyHealth + "/" + maxPartyHealth;
            playerSheild.text = "Player Sheild: " + shield;
            enemyHealth.text = "Enemy Health: " + currentEnemy.currentHealth + "/" + currentEnemy.maxHealth;
            enemySheild.text = "Enemy Sheild: " + currentEnemy.shield;
        }
        else if(currentPartyHealth<=0)
        {
            loseMessage.enabled = true;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.name);
            }
        }
    }

    private void shuffle()
    {
        for (int i = 0; i< discard.Count; i++)
        {
            deck.Add(discard[i]);
            discard.Remove(discard[i]);
        }
        List<Card> temp = new List<Card>();
        deck.Capacity = deck.Count;
        while (deck.Count > 0)
        {
            temp.Add(deck[0]);
            deck.Remove(deck[0]);
        }
        while (temp.Count > 0)
        {
            System.Random rand = new System.Random();
            int pos = rand.Next(0, temp.Count);
            deck.Add(temp[pos]);
            temp.Remove(temp[pos]);
        }
    }

    //not currently going to support sizes other than 3, other sizes to be implemented next sprint
    public void newHand()
    {
        drawCard();
        drawCard();
        drawCard();
    }

    public void drawCard()
    {
        if(deck.Count == 0)
        {
            shuffle();
        }
        hand.Add(deck[0]);
        deck[0].setImage(deck[0].face);
        deck.Remove(deck[0]);

        for (int i = 0; i < hand.Count; i++)
        {
            hand[i].setPosition(-2.5f + (2.5f * i), -3.5f);
            hand[i].setImage(hand[i].face);
        }
    }

    public void playCard(Card c)
    {
        discard.Add(c);
        c.setPosition(50f, 50f);
        c.setImage(null);
        //deal damage up to number of attacks unless the enemy has sheild
        if(c.damage != 0) {
            for (int i = 0; i < c.numberOfAttacks; i++)
            {
                if (currentEnemy.shield > 0)
                {
                    currentEnemy.shield -= 1;
                }
                else
                {
                    currentEnemy.currentHealth -= c.damage;
                    float healthbarnumber = (float)currentPartyHealth / maxPartyHealth;
                    print(healthbarnumber);
                    healthbarnumber = HBN;
                    HealthBar.health = healthbarnumber;
                }
            }
        }

        //shield
        shield += c.defenseStacks;
        //healing - can't go above max health
        if(currentPartyHealth + c.healing > maxPartyHealth)
        {
            currentPartyHealth = maxPartyHealth;
        }
        else
        {
            currentPartyHealth += c.healing;
        }

    }
    private string generateEnemyMessage(Enemy e, Card c)
    {
        string result = "";
        result += e.pool.name + " used " +c.name + ". ";
        if(c.damage > 0)
        {
            result += "It dealt " + c.damage + " damage";
            if(c.numberOfAttacks == 1)
            {
                result += ". ";
            }
            else
            {
                result += " " + c.numberOfAttacks + " times. ";
            }
        }
        if(c.defenseStacks > 0)
        {
            result += e.pool.name + " gained " + c.defenseStacks + " defense. ";
        }
        if (c.healing > 0)
        {
            result += e.pool.name + " gained " + c.healing + " health. ";
        }
        return result;
    }
}
