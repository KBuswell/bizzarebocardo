﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public CardPool pool;
    public List<Card> deck;
    public List<Card> discard;
    public int maxHealth;
    public int currentHealth;
    public int shield;

    // Start is called before the first frame update
    void Start()
    {
        shuffle();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void shuffle()
    {
        for (int i = 0; i < discard.Count; i++)
        {
            deck.Add(discard[i]);
            discard.Remove(discard[i]);
        }
        List<Card> temp = new List<Card>();
        deck.Capacity = deck.Count;
        while (deck.Count > 0)
        {
            temp.Add(deck[0]);
            deck.Remove(deck[0]);
        }
        while (temp.Count > 0)
        {
            System.Random rand = new System.Random();
            int pos = rand.Next(0, temp.Count);
            deck.Add(temp[pos]);
            temp.Remove(temp[pos]);
        }
    }

    public Card enemyCard(PlayerManager party)
    {
        if( deck.Count == 0)
        {
            shuffle();
        }

        //c.setPosition(50f, 50f);
        //c.setImage(null);
        //deal damage up to number of attacks unless the enemy has sheild
        if(deck[0].damage != 0) {
            for (int i = 0; i < deck[0].numberOfAttacks; i++)
            {
                if (party.shield > 0)
                {
                    party.shield -= 1;
                }
                else
                {
                    party.currentPartyHealth -= deck[0].damage;
                }
            }
        }
        //shield
        shield += deck[0].defenseStacks;
        //healing - can't go above max health
        if (currentHealth + deck[0].healing > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else
        {
            currentHealth += deck[0].healing;
        }
        Card played = deck[0];
        discard.Add(deck[0]);
        deck.Remove(deck[0]);
        return played;
    }
}
