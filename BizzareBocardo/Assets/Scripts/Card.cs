﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    public string cardName;
    public int damage;
    public int numberOfAttacks = 1;
    public int defenseStacks;
    public int healing;
    public int draw;
    public Sprite face;
    public SpriteRenderer cardImage;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setImage(Sprite img)
    {
        cardImage.sprite = img;
    }

    public void setName(string name)
    {
        cardName = name;
        gameObject.name = name;
    }

    public void setPosition(float x,float y)
    {
        transform.position = new Vector3(x,y);
    }

/*    public void OnMouseOver()
    {
        if(cardImage.sprite != null)
        {
            if (Input.GetMouseButtonDown(0))
            {

            }
        }
    }*/
}
