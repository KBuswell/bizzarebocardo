﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerHealthBar : MonoBehaviour
{
    Image healthBar;
    float maxHealth = 1f;
    public float health;

    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponent<Image>();

        //health = maxHealth;
    }
    void Update()
    {
        healthBar.fillAmount = health / maxHealth;
    }
}


