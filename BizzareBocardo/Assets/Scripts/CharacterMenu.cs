﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMenu : MonoBehaviour
{
    [SerializeField] private Canvas characterCanvas;
    [SerializeField] private Canvas battleCanvas;
    public Enemy currentEnemy;


    // Start is called before the first frame update
    void Start()
    {
            characterCanvas.enabled = false;

    }


   void Update()
   {
        if (currentEnemy.currentHealth <= 0)
        {
            characterCanvas.enabled = true;
            battleCanvas.enabled = false;
        }
   }

}
