﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGenerator : MonoBehaviour
{
    public List<CardPool> classList;
    public List<CardPool> traitList;
    public Character characterPrefab;
    static int CLASS_CARDS = 3;
    static int TRAIT_CARDS = 2;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Character generateCharacter()
    {
        Character newCharacter = Instantiate(characterPrefab);
        System.Random rand = new System.Random();
        int characterClass = rand.Next(0, classList.Count);
        int characterTrait = rand.Next(0, traitList.Count);
        newCharacter.maxHealth += classList[characterClass].healthModifier;
        newCharacter.maxHealth += traitList[characterTrait].healthModifier;

        for (int i = 0; i < CLASS_CARDS; i++)
        {
            newCharacter.cardSet.Add(Instantiate(classList[characterClass].pool[rand.Next(0, classList[characterClass].pool.Count)]));
            //Kyle is not a programmer exhibit A
        }
        for (int i = 0; i < TRAIT_CARDS; i++)
        {
            newCharacter.cardSet.Add(Instantiate(traitList[characterTrait].pool[rand.Next(0, traitList[characterTrait].pool.Count)]));
        }

        return newCharacter;
    }
}
